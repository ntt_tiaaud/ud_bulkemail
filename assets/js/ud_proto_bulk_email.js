
var windowSizeArray = ["width=1000,height=600,scrollbars=yes" ];


$(document).ready(function(){
	
	$('.myTab').tabs({active: 0});


	$('.newWindow').click(function (event){
 		var url = $(this).attr("href");
		var windowName = "popUp";//$(this).attr("name");
		var windowSize = windowSizeArray[$(this).attr("rel")];
		window.open(url, windowName, windowSize);
		event.preventDefault();

	});


	$('#mktOutMgt').tabs();
	$('#outreaches-tab, #lists-tab').tabs();
	  $('#Send2OMS, #addMoreCust').on('click', function() {
		  $('#sendtoOMS').removeClass('closed');
	  });
	  
	  $(".tglist_cbox").click(function(){
		  var tot_checked =0;
		  $('input:checkbox[name=targetList_checkbox]:checked').each(function() { 
					$('.dummybtn').addClass('hidden'); 
					$('.remtgbtn').removeClass('hidden');
					tot_checked =1;
			 });
			if (tot_checked ==0) {
			$('.dummybtn').removeClass('hidden'); 
			$('.remtgbtn').addClass('hidden');
			}
		}); 
		
		
		
		$('#addCustomer').on('click', function() {
		/*
		   var tabledata='';
		   var last_parent_tblchkbox_id =$('#tbl_targetlists tr:last').find('input[type=checkbox]').attr('id');
		   var next_parent_tblchkbox_id = parseInt(last_parent_tblchkbox_id.replace('targetList_checkbox' , ''))+1;
			$('input:checkbox[name=bobemail]:checked').each(function() { 				
				var nxt_id = 'targetList_checkbox'+next_parent_tblchkbox_id;
				var cN = $(this).closest('tr').find('td:nth-child(2)').html();
				var pin = $(this).closest('tr').find('td:nth-child(3)').html();
				var split_cN = cN.split(',');
				var email = split_cN[0]+'@tiaa-cref.org';
				tabledata = tabledata + "<tr class='hidden' data-sortfiltered='false'><td><input id='"+nxt_id+"' class='tglist_cbox' type='checkbox' name='targetList_checkbox'></td><td>"+pin+"</td><td><a href='mailto:"+email+"'>"+email+"</a></td><td>34234234</td><td></td><td>"+split_cN[1]+"</td><td>"+split_cN[0]+"</td><td></td><td>Michael</td><td>Ramerson</td></tr>";
				next_parent_tblchkbox_id = parseInt(next_parent_tblchkbox_id)+1;
			})
			 $('#tbl_targetlists').append(tabledata);
			 $("#tbl_targetlists").trigger("update");*/
		});
	  
	   $('input:radio[name=outreach_radio]').on('change', function(e) { 
			if($('.new_outreach_tr').length >0) { 
				   $('#appendingrow').html($('#appendrow'));
				   $('.new_outreach_tr').prev().removeClass('shd4');
					$('.new_outreach_tr').remove();
				}
				
		$(this).closest('tr').after("<tr class='new_outreach_tr'><td colspan=7></td></tr>").addClass('shd4');		
		$('.new_outreach_tr td').html($('#appendrow'));
		$('.before_select').addClass('closed');
		$('.after_select').removeClass('closed');
		});		
		
		$('input:radio[name=ad_outreach_radio]').on('change', function(e) { 
			var status =$(this).closest('tr').find('td:eq(3)').html();
			if(status != 'Inactive') {
				$('#inactivate_ORdummy').addClass('hidden');
				$('#inactivate_OR').removeClass('hidden');
			}
			else {
				$('#inactivate_ORdummy').removeClass('hidden');
				$('#inactivate_OR').addClass('hidden');
			}
		});		
		
		$('#inactivate_OR').on('click', function() {
			var outreach = $('input:radio[name=ad_outreach_radio]:checked').closest('tr').find('td:eq(1)').html();
			$('#Inactivate_Template div.body p span').text(outreach);			
		});
		
		$('.inactivateTemp').on('click', function() {
			$('input:radio[name=ad_outreach_radio]:checked').closest('tr').find('td:eq(3)').html('Inactive');
			$('#inactivate_OR').addClass('hidden');
			$('#inactivate_ORdummy').removeClass('hidden');
		});
		
		$('#rejOutreach').on('click', function() {
			$('#rejectOutreach span.txtb').text($('.outreachName').html());
		});
		
		$('#rejReason').on('change', function() {
			if($(this).val() == '') { 
				$('#ConfirmRejOR').addClass('hidden');
				$('#ConfirmRejORdummy').removeClass('hidden');
			}
			else { 
				$('#ConfirmRejOR').removeClass('hidden');
				$('#ConfirmRejORdummy').addClass('hidden');
			}
		});
		
		$('#ConfirmRejORdummy').click(function(){
			
			$(this).attr('href','qcusr_out_reach_management2.html');
			
		});
		
		$('a[href=#addtargetlist-popup]').on('click',function() {
			$('table#tbl_persons2').find('input[type=checkbox]').attr('checked',false);
			$('table#tbl_persons2 th:nth-child(8), table#tbl_persons2 th:nth-child(9), table#tbl_persons2 th:nth-child(10), table#tbl_persons2 td:nth-child(8),table#tbl_persons2 td:nth-child(9), table#tbl_persons2 td:nth-child(10)').addClass('closed');
		});
		
		$('#sortAssets').on('click',function() {
			$('table#tbl_persons2 th:nth-child(8), table#tbl_persons2 th:nth-child(9), table#tbl_persons2 th:nth-child(10), table#tbl_persons2 td:nth-child(8),table#tbl_persons2 td:nth-child(9), table#tbl_persons2 td:nth-child(10)').removeClass('closed');
			$('table#tbl_persons2 th:nth-child(5), table#tbl_persons2 th:nth-child(6), table#tbl_persons2 th:nth-child(7), table#tbl_persons2 td:nth-child(5),table#tbl_persons2 td:nth-child(6), table#tbl_persons2 td:nth-child(7)').addClass('closed');
		});
		
		$('#addCust').on('click',function() {
			var $addChk = $('table#tbl_persons2').find('input[type=checkbox]:checked');
			var cN = $addChk.closest('tr').find('td:nth-child(2)').html().replace(',' , '');
			var cI = $addChk.closest('tr').find('td:nth-child(3)').html();
			var subCN = cN.split(' ');
			var fN = subCN[0];
			var lN = subCN[1];
			var strToAdd =  '<tr><td><input type="checkbox" name="targetList_checkbox" id="targetList_checkbox0"></td><td>0'+cI+'</td>'+
							'<td><a href="mailto:'+fN+'.'+lN+'@tiaa-cref.org">'+fN+'.'+lN+'@tiaa-cref.org</a></td><td>93942342</td>'+
							'<td>Mr</td><td>'+fN+'</td><td>'+lN+'</td><td>Mr</td><td>Michael</td><td>Ramerson</td></tr>';
			$('table#tbl_mylists tr').eq(1).before(strToAdd);
		});
		$('#doneApprove').on('click',function() {
			$('#approveBtn').removeClass('closed');	
			$('#statusChange').html('Pending Data issue');
		});
		$('table#tbl_mylists col:nth-child(1), table#tbl_mylists th:nth-child(1), table#tbl_mylists td:nth-child(1)').addClass('closed');
		
		$('#editListBtn').on('click',function() {
		  $('#nameAlter').html('<input id="List-Name" class="inputxlg" type="text" aria-required="true" value="Mid-west Low-Income Households" name="Name">');				
		  $('table#tbl_mylists col:nth-child(1), table#tbl_mylists th:nth-child(1), table#tbl_mylists td:nth-child(1)').removeClass('closed');
		});
		
		$('#clearfilters').on('click',function() {
			var linktxt = $(this).find('a span.linktxt');
			var sortfld = $('.tblsort input');
			sortfld.hasClass('hidden') ? sortfld.removeClass('hidden') : sortfld.addClass('hidden');
			linktxt.text() =='Clear All Filters'? linktxt.text('Show All Filters') : linktxt.text('Clear All Filters');
		});
		
		$('#showsorting').on('click',function() {
			var linktxt = $(this).find('a');
			$('.tblsort').hasClass('header headerSortDown') || $('.tblsort').hasClass('header headerSortUp')? $('.tblsort').removeClass('header headerSortDown headerSortUp'): $('.tblsort').addClass('header headerSortDown');
			linktxt.text() =='+ Show Sorting'? linktxt.text('- Hide Sorting') : linktxt.text('+ Show Sorting');
		});
		
		$('#conRowRmv').on('click',function() {
			var $remRdoLst = $('table#tbl_mylists11').find('input[type=checkbox]:checked');
			$remRdoLst.closest('tr').addClass('closed');
		});
		
		$('.main_cbox, .mn_cbox, .mn_ORlist_cbox').on('click',function() {
		    var relatedcbox = $(this).attr('id')+'_cbox';
			$(this).is(':checked')? $("."+relatedcbox).prop('checked', true):$("."+relatedcbox).prop('checked', false);
			if($(this).attr('class') =='mn_cbox') switchbtn();
		});
		
		$('.bobemail_cbox, .bobeml_cbox, .ORlist_cbox').on('click',function() {
			   var clss = $(this).attr('class');
			   var main_cbox = clss.replace("_cbox", "");
				$('.'+clss+':checked').length == $('.'+clss).length	 ?
						    	$("#"+main_cbox).prop('checked', true) :
							    $("#"+main_cbox).prop('checked', false);
								
								if($(this).attr('class') =='bobeml_cbox') switchbtn();
		});
		
				
		$('#edit_tgListBtn').on('click',function() {
			$('.tglist_btn,.tglist_secondarybtn').removeClass('closed');
			$('.tglist_cbox').removeClass('hidden');
			$('.tglist_mainbtn').addClass('closed');
		});
		$('#confRemCust').on('click',function() {
            var $remChk = $('table#tbl_targetlists').find('input[type=checkbox]:checked');
            $remChk.closest('tr').addClass('closed');
			$('.dummybtn').removeClass('hidden'); 
			$('.remtgbtn').addClass('hidden');
        });

		$('#ConfirmOutreach').on('click', function() {
			var parent_tr = $('input:radio[name=outreach_radio]:checked').closest('tr');
			parent_tr.find('td:eq(3)').text('Submitted');
		});

		 $('input[name=bobemail]').on('click',function(){	
							var tot_checked =0;			
							$('input:checkbox[name=bobemail]:checked').each(function() {			
										$('#addCust_dummy').addClass('hidden');			
										$('#addCustomer').removeClass('hidden');			
										tot_checked =1;			
							 });			
							if (tot_checked ==0) {			
							$('#addCust_dummy').removeClass('hidden');			
							$('#addCustomer').addClass('hidden');			
							}			
					});
					
		$('input:radio[name=creat_outreach_radio]').on('change', function(e) { 
		  if ($('.subtg').length !=0) {
				$('.sub_tglist').html($('.SubTGLists'));
				$('.subtg').remove();
			}
		});
		
		$('.tglistName').on('click',function() {		
		    if ($('.subtg').length !=0) {
				$('.subtg').prev().find('input:radio[name=creat_outreach_radio]').prop('checked', false);
				$('.sub_tglist').html($('.SubTGLists'));
				$('.subtg').remove();
			}
		   $(this).closest('tr').after("<tr class='subtg'><td></td><td colspan=8 class='subtgList'></td></tr>");
		   $('.subtgList').html($('.SubTGLists'));
		   $(this).closest('tr').find('input:radio[name=creat_outreach_radio]').prop('checked', true);
		});
		
		 $('input:radio[name=template_radio]').on('change', function(e) { 
		    if($(this).closest('tr').find('td:eq(6)').html() !='Active') {
				$('#ConfTempdummy').addClass('hidden');
				$('#ConfTemp').removeClass('hidden');
				$('#InactivateTempdummy').removeClass('hidden');
				$('#InactivateTemp').addClass('hidden');
				}
			else {
				$('#ConfTempdummy').removeClass('hidden');
				$('#ConfTemp').addClass('hidden');
				$('#InactivateTempdummy').addClass('hidden');
				$('#InactivateTemp').removeClass('hidden');
				}
			});
			
		$('#TemplateType').on('change', function() {
		  if($(this).val() =='Marketing') {
			$('.TTEnabled').removeClass('dim').find('label span').removeClass('dim');
			$('.TTEnabled').find('input').attr('disabled',false);
		  }
		  else {
			$('input:radio[name=exclusions]').attr('checked',false);
		    $('.TTEnabled').addClass('dim').find('label span').addClass('dim');
			$('.TTEnabled').find('input').attr('disabled',true);
			$('.ExEnabled').addClass('dim').find('select').attr('disabled',true);
		  }
		});
		
		$('#inactivatetemp').on('click', function() { 
		   $('input:radio[name=template_radio]:checked').closest('tr').find('td:eq(6)').html('Inactive');
		   $('.modal,.modal-overlay').hide();
		   $('#InactivateTempdummy').removeClass('hidden');
		   $('#InactivateTemp').addClass('hidden');
		});
		
		
		
		$('input:radio[name=exclusions]').on('change', function() { 
			if($(this).val() =='Y') $('.ExEnabled').removeClass('dim').find('select').attr('disabled',false);
			else $('.ExEnabled').addClass('dim').find('select').attr('disabled',true);
			});
			
		$('#DefaultActCategory').on('change',function() {
		   if($(this).val() !='') $('.ActCategoryEnabled').removeClass('dim').find('select').attr('disabled',false);
		   else $('.ActCategoryEnabled').addClass('dim').find('select').attr('disabled',true);
		});
		
		$('input:radio[name=Followup]').on('change',function() {
		   if($(this).val() =='yes') {
			   $('.FollowupEnabled').removeClass('dim').find('label span.Fup').removeClass('dim');
			   $('.FollowupEnabled').find('select.Fup').attr('disabled',false);
		   }
		   else {
			$('.FollowupEnabled').addClass('dim').find('label span.Fup').addClass('dim');
			$('.FupAct').addClass('dim').find('label span').addClass('dim');
			$('.FollowupEnabled').find('select').attr('disabled',true);
			$('#FollowupActCategory').val('');
			}
		});
		
		$('#FollowupActCategory').on('change',function() {
		    if($(this).val() !='') {
			$('.FupAct').removeClass('dim').find('label span').removeClass('dim');
			$('.FupAct').attr('disabled',false);
			}
			else {
			$('.FupAct').addClass('dim').find('label span').addClass('dim');
			$('.FupAct').attr('disabled',true);
			}			
		});
		
		$('#configtemplatedone').mouseup(function() { 
		   $('input:radio[name=template_radio]:checked').closest('tr').find('td:eq(6)').html('Partially Configured');
		   var popupObj = $('form[id=ConfigureTemplate]');			
			popupObj.dialog('close');
		});
		
		$('#configTempSave').on('click', function() {
		  var dvalid = $('.dvalidate').length;
			for(i=0;i<=dvalid;i++) {
				if($('.dvalidate:eq('+i+')').val() =='') {
					$('#ConfirmConfigureTemplate').dialog('open');	
					return false;
				}
				else {
					$('input:radio[name=template_radio]:checked').closest('tr').find('td:eq(6)').html('Active');
					$('form[id=ConfigureTemplate]').dialog('close');
					$('#ConfTemp').addClass('hidden');
					$('#ConfTempdummy').removeClass('hidden');
				}
			}
		});
		
		
		$('#InactivateTemp').on('click',function() {
			var tempname = $('input:radio[name=template_radio]:checked').closest('tr').find('td:eq(1)').html();
			$('#InactivateTemplate div.body p span').text(tempname);
		});
});

function switchbtn() {
 var tot_checked =0;
  $('input:checkbox[name=bobemail]:checked').each(function() { 
			$('#addCust_dummy').addClass('hidden'); 
			$('#addCustomer').removeClass('hidden');
			tot_checked =1;
	 });
	if (tot_checked ==0) {
	$('#addCust_dummy').removeClass('hidden'); 
	$('#addCustomer').addClass('hidden');
	}
}